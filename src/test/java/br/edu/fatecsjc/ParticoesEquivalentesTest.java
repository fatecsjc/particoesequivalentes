package br.edu.fatecsjc;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ParticoesEquivalentesTest {

    private ParticoesEquivalentes particoesEquivalentes;

    @Test
    void testarStringVazia() {

        particoesEquivalentes = new ParticoesEquivalentes();
        assertEquals(-2, particoesEquivalentes.testarString(""));
    }

    @Test
    void testarStringMaiorQueCincoCaracteres() {

        particoesEquivalentes = new ParticoesEquivalentes();
        assertEquals(-3, particoesEquivalentes.testarString("cincos"));
    }

    @Test
    void testarStringCaractereNaoEncontrado() {

        particoesEquivalentes = new ParticoesEquivalentes();
        assertEquals(-1, particoesEquivalentes.testarString("nao"));
    }

    @Test
    void testarStringCaractereEncontradoPrimeiroIndice() {

        particoesEquivalentes = new ParticoesEquivalentes();
        assertEquals(0, particoesEquivalentes.testarString("@11"));
    }

    @Test
    void testarStringCaractereEncontradoSegundoIndice() {

        particoesEquivalentes = new ParticoesEquivalentes();
        assertEquals(1, particoesEquivalentes.testarString("1@@"));
    }
}
