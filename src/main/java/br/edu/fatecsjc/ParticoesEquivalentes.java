package br.edu.fatecsjc;

public class ParticoesEquivalentes {

    public int testarString(String word) {

        if (word.length() == 0)
            return -2;
        if (word.length() > 5)
            return -3;
        else
            return word.indexOf('@');
    }
}
